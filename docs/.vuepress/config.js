module.exports = {
  title: 'Flypi',
  dest: 'public',
  base: '/fly-pi/',
  theme: 'vuepress-theme-gocommons',

  themeConfig: {
    projectLogo: "/logo.png",
    projectTitle: "Open Hardware Microscopy",
    nav: [
      { text: 'Intro', link: '/' },
      { text: 'Guide', link: '/Build/' },
      { text: 'License', link: '/LICENSE' },
      { text: 'Contribute', link: '/Contribute/' },
    ],

    sidebar: {
      '/Intro/': [
        '',
        'Model',  /* /foo/one.html */
        'Self-Assesment'   /* /foo/two.html */
      ],

      '/Build/': [
        '',
        'BOM',
        'Design',
        'Step2',
        'Step3',
        'Step4',
        'Step5'
      ],

      '/Contribute/': [
        '',      /* /bar/ */
        'For-devs'
      ],

      // fallback
      '/': [
        ''
      ]
    }

  }
};
