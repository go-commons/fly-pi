---
home: false
sidebar: false
---
<img :src="$withBase('/gif-larva.gif')" alt="Larva" width="100%">

![status](https://img.shields.io/badge/status-beta-orange.svg)
### Designed for flexibiliy and low-cost, the “FlyPi” is an optical microscope designed for behavioural work with fruit flies, zebrafish or C. elegans

The system is based on a Raspberry Pi with camera, an Arduino microcontroller and a range of off-the-shelf electronic parts. A modular design provides for flexible control of a range of peripherals including options for optogenetic and thermogenetic stimulation and fluorescence microscopy. All mechanical parts are 3-D printed which includes a range of manual or motorised micropositioning options.

The basic setup can be put together for below 100€. The low price means that it can be implemented in under-funded labs across the world, for classroom teaching in schools as well as by home-enthusiasts who are not part of the traditional scientific establishment.
</br>
* * *
<img :src="$withBase('/flypi-design.jpg')" alt="Larva" width="100%">
