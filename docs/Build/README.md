# Introduction
In this tutorial, I share the journey of how I built the FlyPi, without any previous electronics experience, so I could feed my insatiable urge to see how things work. Microscopy offers a lens to explore each visual frontier with its wonderful new perspectives and illuminates minuscule life regularly unobserved. This is an amazing experiences for anyone curious of what lies beneath our immediate visual capabilities. One main constraint for curious students, researchers and/or educators to look deeper into the micro world, is access to effective scientific tools. This is likely due to high prices set by development under a patent/scarcity methodology. Building your own FlyPi is a relatively easy and cost effective way to to get around this limiting factor. FlyPi is an all-in-one biology lab with powerful “off-the-shelf” electronic elements (Raspberry Pi & Arduino nano). It's modularity offers a fast, effective and low cost way to have better experimental control by customizing for specific needs and most importantly enables accessibility to research and explore the microscopic world around us. Just putting it together is a great learning experience for everyone not immediately comfortable with electronics. Some People have even begun to join scientific efforts in pursuing solutions to their local problems!

## Materials and tools needed
Monitor

Computer mouse - USB

Keyboard - USB

3D print files (.stl) : Under 'Files' tab above

Custom PCB (Principle Component Board parts can be found on: https://kitspace.org/boards/github.com/prometheus-science/flypi/ )

Tools
Soldering Iron, Solder

Computer with:

- SD card reader

- Internet connection

3D printer

## Steps

1- Understanding Modularity: Deciding what to include or exclude in the build

2- Gathering materials & Creating parts: Options to buy parts from kitspace, link to original github repository

Due to modularity, not all parts are necessary for basic functionality. (Repository has more detailed bill of material files)

3- Building PCB hardware: Quick overview of the PCB build

4- Software Installation: How to install interface and useful imagej FIJI software on SD card

5- Launching the FlyPi: Putting it all together to start collecting image data!
