# 3. Building PCB board

## PCB architecture overview
A fully populated port should look like the one in the image

![PCB](../images/pcb_1.jpg)

The PCB board is organized in the following modules:

a) Power 1 - 12v to 5v converter dedicated to power the Raspberry Pi

b) Power 2 - 12v to 5v converter to power LEDs, LED ring, Thermistor and devices on the I2C port

c) High power LED / 5V general module 1 - Port that can be used with high power LEDs or any other component that takes up to 5V (uses a NPN transistor, PWM capable).

d) High power LED / 5V general module 2 - Port that can be used with high power LEDs or any other component that takes up to 5V.

c) LED ring module - used to power and control the LED ring

d) servo motor module - used to power and control a continuous servo motor

e) Peltier element module - comprised of fan, RGB led, thermistor and peltier element ports.

## How the user orders the PCB?

## Can we put any references to soldering tutorials or tips?
