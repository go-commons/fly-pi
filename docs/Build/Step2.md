# 2. Gathering Materials and creating the parts
Step 2 - Gathering Materials & Creating Parts
2- Gathering materials & Creating Parts: Not all parts are necessary to order for basic functionality. The repository has a detailed bill of materials that can be tailored to specific needs.

Electronic parts can be ordered from this kitspace page (lists have been conveniently compiled in various online market shopping carts):
https://kitspace.org/boards/github.com/prometheus-science/flypi/

3D print files can be found in the files tab above. I started printing at the local fablab (fabrication laboratory) after ordering the parts I needed to build the microscope. This way I would have the body ready when the electronics arrived.
