# Bill of Materials

| Quantity   |      Component name      |  Notes |
|:----------:|:-------------:|:------:|
| 1 |  	Raspberry Pi 2 or 3 | |
| 1 |   16 GB mini SD card |   card is installed with raspian|
| 1 | HDMI to VGA adapter |    |
| 1  | Monitor |    |
| 1 |	Mouse (USB) |    |
| 1 | Keyboard (USB) |    |
| 1 | Arduino nano ATmega328 | Controls all periohericals except camera   |
| 1 | Custom printed circuit board (PCB) | makes the connection between Arduino and periphericals easier   |
| 1 | Mainframe and holders for camera, led ring and etc | 3D printed parts    |
| 1 | Adjustable focus RPi Camera | http://www.wvshare.com/product/RPi-Camera-B.htm   |
| 1 | 	USB A to mini B cable |  connects arduino Nano to the raspberry pi  |
| 1 |  RGB LED | marker for peltier  |
| 3 |220 OHM resistors  | For RGB LEDs   |
| 1| Paper sheet|Diffuse light to evenly illuminate samples|
| 1 | Adafruit 8X8 blue LED matrix | https://www.adafruit.com/products/1052 |
|1 |	Adafruit 12 LED ring | https://www.adafruit.com/products/1643 |
|1 |100 nF capacitor | filter for the LED ring (together with resistor below) |
|1 |150 Ohm resistor |filter for LED ring |
| 1 | AD22100 Temperature sensor | measures temperature in the peltier element |
| 1  | 1k Ohm resistor | filter for temperature sensor|
| 1 | 100nF capacitor | filter for temperature sensor |
| 1 | Peltier element TEC1 127 05 (40X40X4mm) | |
| 1 | Heat sink for Peltier (35x35x20MM)| |
| 1 | Fan for Peltier (40X40X10mm 5V)| |
| 1 | H-brigde L298N | https://www.sparkfun.com/products/9479|
| 1 | heat sink for H-bridge | right now, 40X40X10, but this maybe an overshoot |
